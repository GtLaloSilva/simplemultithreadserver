/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package simplemultithreadserver;

import java.util.Scanner;
import simplemultithreadserver.controller.Server;

/**
 *
 * @author Lalo
 */
public class SimpleMultiThreadServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Crear el servidor y ejecutarlo.
        Server server = new Server();
        
        Scanner reader = new Scanner(System.in);
        
        System.out.println("########### Servidor Multi-Hilo ###########");
        System.out.format("# Autor: %32s #\n", "Eduardo G. Silva");
        System.out.format("# Version: %30s #\n", "1.0");
        System.out.println("###########################################");
        
        int command = -1;
        
        boolean active = true;
        
        while(active){
            System.out.println("Elija una opción:");
            System.out.println("1. "+ ((!server.isRunning())?"Iniciar ":"Detener") + " el servidor.");
            System.out.println("2. Salir.");
            
            System.out.print("\n: ");
            
            command = reader.nextInt();
            
            switch(command){
                case 1:
                    if(!server.isRunning()){
                        server.startServer();
                    }else{
                        server.shutDownServer();
                    }
                    break;
                case 2:
                    if(server.isRunning()){
                        server.shutDownServer();
                    }
                    active = false;
                    break;
                default:
                    System.out.println("Opción desconocida");
                    break;
            }
            
        }
        
    }
    
}
