/*
 * Clase principal para el servidor multi hilo.
 */

package simplemultithreadserver.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase empleada para gestionar las peticiones entrantes.
 * @author Eduardo Gutiérrez Silva
 * @version 0.1
 * TEGAP
 */
public class Server implements Runnable{
    /* ############ CONSTANTES DEL SERVIDOR ############ */
    private final static String SERVER_CLASS_NAME = Server.class.getName();
    
    /* ############ PARÁMETROS DE CONFIGURACIÓN PARA EL SERVIDOR ############ */
    // Puerto de escucha.
    private int port = 1234;
    // Número máximo de conexiones pendientes para el socket.
    private int backlog = 10;
    // Dirección de red en la que se escucha.
    private InetAddress serverAddress = null;
    // Número de hilos que pueden estarse ejecutando.
    private int threadPoolSize = 10;
    
    /* ############ MIEMBROS DEL SERVIDOR ############ */
    // Hilo en el que se ejecuta el servidor.
    private Thread serverThread;
    
    // Socket para el servidor.
    private ServerSocket serverSocket;  
    
    // Limita el número de hilos que la aplicación puede ejecutar para los clientes.
    private ExecutorService executorService = null;
    
    List<ClientListener> clientes = null;
    
    /* ############ EJECUCIÓN PRINCIPAL ############ */
    @Override
    public void run() {
        
        try {
            Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "Obteniendo dirección IP…");
            
            // Obteniendo dirección IP.
            serverAddress = InetAddress.getLocalHost();
            
            Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "✔");
            
            
            Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "Conectando el socket del servidor…");
            
            // Opening ports and binding address.
            serverSocket = new ServerSocket(port, backlog, serverAddress);
            
            Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "✔");
            
            clientes = new ArrayList<>();
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(SERVER_CLASS_NAME).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SERVER_CLASS_NAME).log(Level.SEVERE, null, ex);
        }
        
        // Inicializando el servicio de ejecución.
        executorService = Executors.newFixedThreadPool(threadPoolSize);
        
        try{
            // Ciclo principal de la aplicación.
            while(!Thread.interrupted()){
                
                // Escuchar peticiones entrantes y generar el socket para el cliente.
                Socket clientSocket = serverSocket.accept();
                
                Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "Petici\u00f3n entrante de: {0}", clientSocket.getRemoteSocketAddress());
                
                // Generar el hilo para escuchar los mensajes del cliente.
                ClientListener cl = new ClientListener(clientSocket);
                clientes.add(cl);
                executorService.execute(cl);
            }
        } catch (IOException ex) {
            Logger.getLogger(SERVER_CLASS_NAME).log(Level.SEVERE, null, ex);
        }finally{
            try {
                serverSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    } // Termina run de Server.
    
    /**
     * Inicia el servidor en su propio hilo.
     */
    public void startServer(){
        serverThread = new Thread(this);
        serverThread.start();
    }
    
    /**
     * Termina la ejecución del servidor.
     */
    public void shutDownServer(){
        Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "Terminando clientes…");
        for(ClientListener cl : clientes){
            cl.stop();
        }
        Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "✔");
        
        Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "Terminando ExecutorService…");
        executorService.shutdownNow();
        Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "✔");
        
        Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "Terminando Servidor…");
        
        try {
            // Detener el socket del server
            serverSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Terminando servidor interrumpiendo el hilo.
        serverThread.interrupt();
        
        try{
            serverThread.join();
        }catch(InterruptedException ex){
            // Interrupción inesperada.
            Logger.getLogger(SERVER_CLASS_NAME).log(Level.SEVERE, null, ex);
            System.exit(1);
        }finally{
            serverThread = null;
        }
        Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "✔");
    }
    
    /**
     * Clase para el control de la conexión con los clientes.
     */
    private class ClientListener implements Runnable {

        // Socket para la comunicación con el cliente.
        private Socket clientSocket = null;
        
        private String command = null;
        
        public ClientListener(Socket clientSocket){
            this.clientSocket = clientSocket;
        }
        
        @Override
        public void run() {
            
            // Permite el envío de datos al cliente.
            PrintWriter writer = null;
            // Permite la lectura de los datos del cliente.
            BufferedReader reader = null;
            
            try {
                
                // Instanciado Entrada / Salida (I/O).
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                writer = new PrintWriter(clientSocket.getOutputStream(),true);
                
                while(!Thread.interrupted()){
                    // Esta función bloquea hasta que se recibe algún dato o se cierra la conexión.
                    command = reader.readLine();
                    if(command == null){
                        Logger.getLogger(SERVER_CLASS_NAME).log(Level.INFO, "Finalizada conexi\u00f3n con cliente desde: {0}", clientSocket.getRemoteSocketAddress());
                        break;
                    }
                    processCommand(command);
                }
            } catch (IOException ex) {
                Logger.getLogger(SERVER_CLASS_NAME).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    reader.close();
                    writer.close();
                    clientSocket.close();
                } catch (IOException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }    
            }
        }

        private void processCommand(String command) {
            System.out.println("Comando: " + command);
        }
        
        public void stop(){
            if(clientSocket!=null){
                try {
                    clientSocket.close();
                } catch (IOException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }// Termina ClientListener.

    
    /* ############ SETTERS & GETTERS ############ */
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getBacklog() {
        return backlog;
    }

    public void setBacklog(int backlog) {
        this.backlog = backlog;
    }

    public InetAddress getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(InetAddress serverAddress) {
        this.serverAddress = serverAddress;
    }

    public int getThreadPoolSize() {
        return threadPoolSize;
    }

    public void setThreadPoolSize(int threadPoolSize) {
        this.threadPoolSize = threadPoolSize;
    }
    
    public boolean isRunning(){
        return (serverThread != null && serverThread.isAlive());
    }
}
